import unittest
from resistor import decodeTolerance


class TestDecodeTolerance(unittest.TestCase):

    def testBrownTolerance(self):
        colors = ['red', 'red', 'red', 'brown']
        self.assertEquals(1.0, decodeTolerance(colors))

    def testRedTolerance(self):
        colors = ['red', 'red', 'red', 'red']
        self.assertEquals(2.0, decodeTolerance(colors))

    def testGreenTolerance(self):
        colors = ['red', 'red', 'red', 'green']
        self.assertEquals(0.5, decodeTolerance(colors))

    def testBlueTolerance(self):
        colors = ['red', 'red', 'red', 'blue']
        self.assertEquals(0.25, decodeTolerance(colors))

    def testVioletTolerance(self):
        colors = ['red', 'red', 'red', 'violet']
        self.assertEquals(0.1, decodeTolerance(colors))

    def testGreyTolerance(self):
        colors = ['red', 'red', 'red', 'grey']
        self.assertEquals(0.05, decodeTolerance(colors))

    def testGoldTolerance(self):
        colors = ['red', 'red', 'red', 'gold']
        self.assertEquals(5.0, decodeTolerance(colors))

    def testSilverTolerance(self):
        colors = ['red', 'red', 'red', 'silver']
        self.assertEquals(10.0, decodeTolerance(colors))

    def testNoneTolerance(self):
        colors = ['red', 'red', 'red', 'none']
        self.assertEquals(20.0, decodeTolerance(colors))
