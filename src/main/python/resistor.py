__validColorsForSignificantFigures = [
    'black', 'brown', 'red', 'orange', 'yellow', 'green',
    'blue', 'violet', 'grey', 'white']

__toleranceValues = {
    'brown': 1.0,
    'red': 2.0,
    'green': 0.5,
    'blue': 0.25,
    'violet': 0.1,
    'grey': 0.05,
    'gold': 5.0,
    'silver': 10.0,
    'none': 20.0}

__multiplierValues = {
    'black': (1, ''),
    'brown': (10, ''),
    'red': (100, ''),
    'orange': (1, 'K'),
    'yellow': (10, 'K'),
    'green': (100, 'K'),
    'blue': (1, 'M'),
    'violet': (10, 'M'),
    'grey': (100, 'M'),
    'white': (1, 'G'),
    'gold': (0.1, ''),
    'silver': (0.01, '')}


def validateColorsList(colors):
    if len(colors) < 4 or len(colors) > 5:
        raise ValueError('there must only be 4 or 5 bands')

    if __getToleranceBandColor(colors) not in __toleranceValues:
        raise ValueError('tolerance band invalid')

    if __getMultiplyBandColor(colors) not in __multiplierValues:
        raise ValueError('multiply band invalid')

    for color in __getSignificantFigureBandColors(colors):
        if color not in __validColorsForSignificantFigures:
            raise ValueError(f"{color} is not a valid significant figure color")


def decodeTolerance(colors):
    toleranceColor = __getToleranceBandColor(colors)
    return __toleranceValues.get(toleranceColor)


def decodeMultiplier(colors):
    multiplierColor = __getMultiplyBandColor(colors)
    return __multiplierValues.get(multiplierColor)


def decodeSignificantFigures(colors):
    signigicantFigureColors = __getSignificantFigureBandColors(colors)
    numericPortionString = ''
    for color in signigicantFigureColors:
        numericPortionString += str(__validColorsForSignificantFigures.index(color))

    return int(numericPortionString)


def __getToleranceBandColor(colors):
    return colors[-1]


def __getMultiplyBandColor(colors):
    return colors[-2]


def __getSignificantFigureBandColors(colors):
    return colors[0:-2]


def createFormattedResistanceString(colors):
    resistanceParts = __getResistanceParts(colors)

    resistorDescription = f"{resistanceParts.get('value')} " + \
        f"{resistanceParts.get('units')} +/- {resistanceParts.get('tolerance')}%"

    return resistorDescription


def decodeResistance(colors):
    decodedResistance = __getResistanceParts(colors)
    decodedResistance.update({'formatted': createFormattedResistanceString(colors)})
    return decodedResistance


def __getResistanceParts(colors):
    significantFigures = decodeSignificantFigures(colors)
    (multiplierNumeric, multiplierPrefix) = decodeMultiplier(colors)
    tolerance = decodeTolerance(colors)

    resistanceParts = {
        'value': significantFigures * multiplierNumeric,
        'units': f"{multiplierPrefix}ohms",
        'tolerance': tolerance}

    return resistanceParts
