import unittest
from resistor import decodeResistance


class TestDecodeResistance(unittest.TestCase):

    def testNoMultiplierPrefix(self):
        colors = ['brown', 'red', 'orange', 'black', 'brown']

        expected = {
            'value': 123,
            'units': 'ohms',
            'tolerance': 1.0,
            'formatted': '123 ohms +/- 1.0%'}

        self.assertEquals(expected, decodeResistance(colors))

    def testKMultiplierPrefix(self):
        colors = ['brown', 'red', 'orange', 'yellow', 'red']

        expected = {
            'value': 1230,
            'units': 'Kohms',
            'tolerance': 2.0,
            'formatted': '1230 Kohms +/- 2.0%'}

        self.assertEquals(expected, decodeResistance(colors))

    def testMMultiplierPrefix(self):
        colors = ['brown', 'red', 'orange', 'grey', 'green']

        expected = {
            'value': 12300,
            'units': 'Mohms',
            'tolerance': 0.5,
            'formatted': '12300 Mohms +/- 0.5%'}

        self.assertEquals(expected, decodeResistance(colors))

    def testGMultiplierPrefix(self):
        colors = ['brown', 'red', 'orange', 'white', 'grey']

        expected = {
            'value': 123,
            'units': 'Gohms',
            'tolerance': 0.05,
            'formatted': '123 Gohms +/- 0.05%'}

        self.assertEquals(expected, decodeResistance(colors))

    def testDecimalMultiplier(self):
        colors = ['brown', 'red', 'orange', 'gold', 'none']

        expected = {
            'value': 12.3,
            'units': 'ohms',
            'tolerance': 20.0,
            'formatted': '12.3 ohms +/- 20.0%'}

        self.assertEquals(expected, decodeResistance(colors))
