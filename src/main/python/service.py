from http.server import BaseHTTPRequestHandler, HTTPServer
from urllib.parse import urlparse
import re
import resistor


class ResistorHTTPRequestHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        self.log_message("path: %s", self.path)
        try:
            path = self.path
            if not path.startswith('/decode'):
                self.log_message("resource: " + path)
                self.send_error(404)
            colors = self.__parseColors(path)
            resistor.validateColorsList(colors)
            self.__generateBody(colors)
        except Exception as e:
            self.send_error(400, str(e))

    def __parseColors(self, path):
        queryParts = urlparse(self.path).query.split('&')
        colors = []

        for part in queryParts:
            color = re.sub(r'band\d=', '', part)
            colors.append(color)

        return colors

    def __generateBody(self, colors):
        body = resistor.createFormattedResistanceString(colors)
        self.send_response(200)
        self.send_header('Content-Type', 'text/html')
        self.end_headers()
        self.wfile.write(bytes(body, 'UTF-8'))


def startServer():
    print("webserver running on port 8000")
    daemon = HTTPServer(('', 8000), ResistorHTTPRequestHandler)
    daemon.serve_forever()


if __name__ == '__main__':
    startServer()
