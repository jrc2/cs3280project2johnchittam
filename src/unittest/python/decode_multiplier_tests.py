import unittest
from resistor import decodeMultiplier


class TestDecodeMultiplier(unittest.TestCase):

    def testBlackMultiplier(self):
        colors = ['red', 'red', 'black', 'brown']
        self.assertEquals((1, ''), decodeMultiplier(colors))

    def testBrownMultiplier(self):
        colors = ['red', 'red', 'brown', 'brown']
        self.assertEquals((10, ''), decodeMultiplier(colors))

    def testRedMultiplier(self):
        colors = ['red', 'red', 'red', 'brown']
        self.assertEquals((100, ''), decodeMultiplier(colors))

    def testOrangeMultiplier(self):
        colors = ['red', 'red', 'orange', 'brown']
        self.assertEquals((1, 'K'), decodeMultiplier(colors))

    def testYellowMultiplier(self):
        colors = ['red', 'red', 'yellow', 'brown']
        self.assertEquals((10, 'K'), decodeMultiplier(colors))

    def testGreenMultiplier(self):
        colors = ['red', 'red', 'green', 'brown']
        self.assertEquals((100, 'K'), decodeMultiplier(colors))

    def testBlueMultiplier(self):
        colors = ['red', 'red', 'blue', 'brown']
        self.assertEquals((1, 'M'), decodeMultiplier(colors))

    def testVioletMultiplier(self):
        colors = ['red', 'red', 'violet', 'brown']
        self.assertEquals((10, 'M'), decodeMultiplier(colors))

    def testGreyMultiplier(self):
        colors = ['red', 'red', 'grey', 'brown']
        self.assertEquals((100, 'M'), decodeMultiplier(colors))

    def testWhiteMultiplier(self):
        colors = ['red', 'red', 'white', 'brown']
        self.assertEquals((1, 'G'), decodeMultiplier(colors))

    def testGoldMultiplier(self):
        colors = ['red', 'red', 'gold', 'brown']
        self.assertEquals((0.1, ''), decodeMultiplier(colors))

    def testSilverMultiplier(self):
        colors = ['red', 'red', 'silver', 'brown']
        self.assertEquals((0.01, ''), decodeMultiplier(colors))
