import unittest
from resistor import createFormattedResistanceString


class TestCreateFormattedResistanceString(unittest.TestCase):

    def testNoMultiplierPrefix(self):
        colors = ['brown', 'red', 'orange', 'black', 'brown']
        self.assertEquals('123 ohms +/- 1.0%', createFormattedResistanceString(colors))

    def testKMultiplierPrefix(self):
        colors = ['brown', 'red', 'orange', 'yellow', 'red']
        self.assertEquals('1230 Kohms +/- 2.0%', createFormattedResistanceString(colors))

    def testMMultiplierPrefix(self):
        colors = ['brown', 'red', 'orange', 'grey', 'green']
        self.assertEquals('12300 Mohms +/- 0.5%', createFormattedResistanceString(colors))

    def testGMultiplierPrefix(self):
        colors = ['brown', 'red', 'orange', 'white', 'grey']
        self.assertEquals('123 Gohms +/- 0.05%', createFormattedResistanceString(colors))

    def testDecimalMultiplier(self):
        colors = ['brown', 'red', 'orange', 'gold', 'none']
        self.assertEquals('12.3 ohms +/- 20.0%', createFormattedResistanceString(colors))
