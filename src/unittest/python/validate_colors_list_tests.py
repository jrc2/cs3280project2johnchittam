import unittest
from resistor import validateColorsList


class TestValidateColorsList(unittest.TestCase):

    def testEmptyListException(self):
        self.assertRaises(ValueError, validateColorsList, [])

    def testTooFewColorsException(self):
        colors = ['brown', 'blue', 'red']
        self.assertRaises(ValueError, validateColorsList, colors)

    def testTooManyColorsException(self):
        colors = ['brown', 'blue', 'red', 'brown', 'blue', 'red']
        self.assertRaises(ValueError, validateColorsList, colors)

    def testInvalidToleranceException(self):
        colors = ['black', 'brown', 'red', 'black']
        self.assertRaises(ValueError, validateColorsList, colors)

    def testInvalidMultiplyBandException(self):
        colors = ['black', 'brown', 'none', 'brown']
        self.assertRaises(ValueError, validateColorsList, colors)

    def testInvalidSignificantFigureFirstBand(self):
        colors = ['gold', 'brown', 'green', 'brown']
        self.assertRaises(ValueError, validateColorsList, colors)

    def testInvalidSigificantFigureSecondBand(self):
        colors = ['yellow', 'silver', 'green', 'brown']
        self.assertRaises(ValueError, validateColorsList, colors)

    def testInvalidSiginificantFigureThirdBand(self):
        colors = ['grey', 'brown', 'none', 'green', 'brown']
        self.assertRaises(ValueError, validateColorsList, colors)

    def testShouldAllowBlackBrownRedSigValuesBlackMultiplyBrownTolerance(self):
        colors = ['black', 'brown', 'red', 'black', 'brown']
        try:
            validateColorsList(colors)
        except ValueError as error:
            self.fail(error)

    def testShouldAllowOrangeYellowGreenSigValuesBrownMultiplyRedTolerance(self):
        colors = ['orange', 'yellow', 'green', 'brown', 'red']
        try:
            validateColorsList(colors)
        except ValueError as error:
            self.fail(error)

    def testShouldAllowBlueVioletGreySigValuesRedMultiplyGreenTolerance(self):
        colors = ['blue', 'violet', 'grey', 'red', 'green']
        try:
            validateColorsList(colors)
        except ValueError as error:
            self.fail(error)

    def testShouldAllowWhiteSigValueOrangeMultiplyBlueTolerance(self):
        colors = ['orange', 'yellow', 'white', 'orange', 'blue']
        try:
            validateColorsList(colors)
        except ValueError as error:
            self.fail(error)

    def testShouldAllowYellowMultiplyVioletTolerance(self):
        colors = ['orange', 'yellow', 'white', 'yellow', 'violet']
        try:
            validateColorsList(colors)
        except ValueError as error:
            self.fail(error)

    def testShouldAllowGreenMultiplyGreyTolerance(self):
        colors = ['orange', 'yellow', 'white', 'green', 'grey']
        try:
            validateColorsList(colors)
        except ValueError as error:
            self.fail(error)

    def testShouldAllowBlueMultiplyGoldTolerance(self):
        colors = ['orange', 'yellow', 'white', 'blue', 'gold']
        try:
            validateColorsList(colors)
        except ValueError as error:
            self.fail(error)

    def testShouldAllowVioletMultiplySilverTolerance(self):
        colors = ['orange', 'yellow', 'white', 'violet', 'silver']
        try:
            validateColorsList(colors)
        except ValueError as error:
            self.fail(error)

    def testShouldAllowGreyMultiplyNoneTolerance(self):
        colors = ['orange', 'yellow', 'white', 'grey', 'none']
        try:
            validateColorsList(colors)
        except ValueError as error:
            self.fail(error)

    def testShouldAllowWhiteMultiply(self):
        colors = ['orange', 'yellow', 'white', 'white', 'none']
        try:
            validateColorsList(colors)
        except ValueError as error:
            self.fail(error)

    def testShouldAllowGoldMultiply(self):
        colors = ['orange', 'yellow', 'white', 'gold', 'none']
        try:
            validateColorsList(colors)
        except ValueError as error:
            self.fail(error)

    def testShouldAllowSilverMultiply(self):
        colors = ['orange', 'yellow', 'white', 'silver', 'none']
        try:
            validateColorsList(colors)
        except ValueError as error:
            self.fail(error)
