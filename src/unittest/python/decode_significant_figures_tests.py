import unittest
from resistor import decodeSignificantFigures


class TestDecodeSignificantFigures(unittest.TestCase):

    def testTwoSignificantFigures(self):
        colors = ['black', 'brown', 'violet', 'grey']
        self.assertEquals(1, decodeSignificantFigures(colors))

    def testRedOrangeYellow(self):
        colors = ['red', 'orange', 'yellow', 'violet', 'grey']
        self.assertEquals(234, decodeSignificantFigures(colors))

    def testGreenBlueViolet(self):
        colors = ['green', 'blue', 'violet', 'violet', 'grey']
        self.assertEquals(567, decodeSignificantFigures(colors))

    def testGreyWhite(self):
        colors = ['grey', 'white', 'violet', 'grey']
        self.assertEquals(89, decodeSignificantFigures(colors))
